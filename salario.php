<?php

class empleado {

 private $nombre;
 private $apellido;
 private $isss;
 private $renta;
 private $afp;
 private $sueldoNominal;
 private $sueldoLiquido;
 private $pagoxhoraextra;

 const ISSS = 0.05;
 const RENTA = 0.075;
 const AFP = 0.085;

 function __construct(){
  $this->nombre = "";
 $this->apellido = "";
 $this->sueldoLiquido = 0.0;
 $this->pagoxhoraextra = 10.0;
 }

 function __destruct(){

 echo "\n<p>\n<a href=\"sueldo.php\">Volver</a>\n</p>\n";
 }


 function obtenerSalarioNeto($nombre, $apellido, $salario, $horasExtras){
 $this->nombre = $nombre;
 $this->apellido = $apellido;
 $this->isss = ($salario + $horasExtras*$this->pagoxhoraextra) * self::ISSS;
 $this->renta = ($salario + $horasExtras*$this->pagoxhoraextra) * self::RENTA;
 $this->afp = ($salario + $horasExtras*$this->pagoxhoraextra) * self::AFP;
 $this->sueldoNominal = $salario; 
 $this->sueldoLiquido = $salario + $horasExtras*$this->pagoxhoraextra - ($this->isss +
$this->renta + $this->afp);
 $this->imprimirBoletaPago($this->nombre, $this->apellido, $this->isss, $this->renta, $this->afp, $this->sueldoNominal, $horasExtras*$this-> pagoxhoraextra,
$this->sueldoLiquido);
 }
 function imprimirBoletaPago($nombre, $apellido, $isss, $renta, $afp,
$salarioNominal, $salarioHorasExtras, $salarioNeto){
 $tabla = "<table>\n<tr>\n";
$tabla .= "<tr>\n<td>Nombre empleado: </td>\n";
 $tabla .= "<td>$nombre" . " " . "$apellido</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Salario nominal: </td>\n";
 $tabla .= "<td>$ " . number_format($salarioNominal, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Salario por horas extras: </td>\n";
 $tabla .= "<td>$ " . number_format($salarioHorasExtras, 2, '.', ',') ."</td>\n</tr>\n";
 $tabla .= "<tr>\n<td colspan=\"2\">Descuentos</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Descuento seguro social: </td>\n";
 $tabla .= "<td>$ " . number_format($isss, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Descuento renta: </td>\n";
 $tabla .= "<td>$ " . number_format($renta, 2, '.', ',') . "</td>\n</tr>";
 $tabla .= "<tr>\n<td>Descuento AFP: </td>\n";
 $tabla .= "<td>$ " . number_format($afp, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Total descuentos: </td>\n";
 $tabla .= "<td>$ " . number_format($isss + $renta + $afp, 2, '.', ',') .
"</td>\n</tr>\n";
 $tabla .= "<tr>\n<td>Sueldo l&iacute;quido a pagar: </td>\n";
 $tabla .= "<td> $" . number_format($salarioNeto, 2, '.', ',') . "</td>\n</tr>\n";
 $tabla .= "</table>\n";
 echo $tabla;
 }
 }
?> 